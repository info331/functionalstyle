import org.junit.Test;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class HostFiltering {

    @Test
    public void filterHosts() {

        ArrayList<Host> hosts = new ArrayList<>();
        hosts.add(new Host("Host1", "192.168.0.1"));
        hosts.add(new Host("Host2", "192.168.0.2"));
        hosts.add(new Host("Host3", "192.168.0.3"));
        hosts.add(new Host("Host4", "192.168.0.4"));
        hosts.add(new Host("Host5", "192.168.0.5"));

        List<String> blackListedIps = Arrays.asList("192.168.0.3", "192.168.0.4");

        String filteredResult = hosts.stream()
                .filter(host -> !blackListedIps.contains(host.getIp()))
                .map(host -> host.getName().toLowerCase())
                .reduce((lst, elem) -> lst + ", " + elem)
                .orElse("");

        System.out.println(filteredResult);
    }
}